package main

import (
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"time"
)

var clear map[string]func()

func clearUnix() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func init() {
	clear = make(map[string]func())
	clear["linux"] = func() {
		clearUnix()
	}

	clear["darwin"] = func() {
		clearUnix()
	}

	clear["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}

func callClear() {
	value, ok := clear[runtime.GOOS]
	if ok {
		value()
	} else {
		panic("OS unsupported!")
	}
}

func main() {
	fmt.Println("This will dissapear")
	time.Sleep(2 * time.Second)
	callClear()
	fmt.Println("After clear")
}
